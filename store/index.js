export const state = () => ({
    page: 'index'
})

export const mutations = {
    updatePage(state, pageName) {
        state.page = pageName
    }
}




// import Vuex from 'vuex'

// const createstore = () => {
//     return new Vuex.Store({
//         state: {
//             page: 'index'
//         },
//         mutations: {
//             updatePage(state, pageName) {
//                 state.page = pageName
//             }
//         }
//     })
// }

// export default createstore